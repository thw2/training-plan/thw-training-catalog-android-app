package de.danisbu.thw.trainingplan.android.persistence.io.room.dao

import androidx.room.*
import de.danisbu.thw.trainingplan.android.persistence.io.room.entities.TrainingEntity
import de.danisbu.thw.trainingplan.android.persistence.io.room.entities.TrainingWithCourseEntity

@Dao
interface TrainingEntityDao {
    @Query("SELECT * FROM TrainingEntity")
    fun getAllTrainingEntitiesWithCourses(): List<TrainingWithCourseEntity>

    @Insert
    fun insert(vararg trainingEntity: TrainingEntity)

    @Update
    fun update(vararg trainingEntity: TrainingEntity)

    @Delete
    fun delete(user: TrainingEntity)

    @Query("SELECT * FROM TrainingEntity WHERE id LIKE :id")
    fun getByTrainingId(id: String): List<TrainingWithCourseEntity>
}