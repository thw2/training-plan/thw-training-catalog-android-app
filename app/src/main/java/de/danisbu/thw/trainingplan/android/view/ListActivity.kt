package de.danisbu.thw.trainingplan.android.view


import android.os.Bundle
import android.view.View
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import de.danisbu.thw.trainingplan.android.R
import de.danisbu.thw.trainingplan.android.connector.DummyConnector
import de.danisbu.thw.trainingplan.android.entity.Alert
import de.danisbu.thw.trainingplan.android.entity.TrainingAndAlert
import de.danisbu.thw.trainingplan.android.persistence.SingleAlertRepository
import de.danisbu.thw.trainingplan.android.view.adapter.TrainingListAdapter
import de.danisbu.thw.trainingplan.modell.TrainingList


class ListActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        val myListView = findViewById<View>(R.id.training_list_view) as ListView
        val singleAlertRepository = SingleAlertRepository.createFromContext(this.baseContext)

        this.updateLocalTrainingList(DummyConnector().getFullTrainingList(), singleAlertRepository)

        myListView.adapter = TrainingListAdapter(
            singleAlertRepository.findAll(),
            singleAlertRepository,
            this
        )
    }

    //refactor
    private fun updateLocalTrainingList(
        fullTrainingList: TrainingList,
        singleAlertRepository: SingleAlertRepository
    ) {
        // write test for function
        val localTrainingAndAlertList = singleAlertRepository.findAll()

        localTrainingAndAlertList.list.forEach {
            if(fullTrainingList.list.contains(it.training)){
                fullTrainingList.list.remove(it.training)
            } else {
                singleAlertRepository.remove(it)
            }
        }

        fullTrainingList.list.forEach {
            singleAlertRepository.addOrUpdate(TrainingAndAlert(it, Alert(false)))
        }
    }
}