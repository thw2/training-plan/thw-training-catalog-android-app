package de.danisbu.thw.trainingplan.android.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import de.danisbu.thw.trainingplan.android.R
import de.danisbu.thw.trainingplan.android.entity.Alert
import de.danisbu.thw.trainingplan.android.entity.TrainingAndAlert
import de.danisbu.thw.trainingplan.android.entity.TrainingAndAlertList
import de.danisbu.thw.trainingplan.android.persistence.SingleAlertRepository


class TrainingListAdapter(
    data: TrainingAndAlertList,
    private val singleAlertRepository: SingleAlertRepository,
    context: Context
) : ArrayAdapter<TrainingAndAlert>(context, R.layout.list_row, data.list) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val trainingAndAlert = (getItem(position) as TrainingAndAlert)
        val training = trainingAndAlert.training

        val currentConvertView: View = if(convertView == null){
            val layoutInflater = LayoutInflater.from(context)
            layoutInflater.inflate(R.layout.list_row, parent, false)
        } else {
            convertView
        }

        currentConvertView.findViewById<TextView>(R.id.location).text = training.location.value
        currentConvertView.findViewById<TextView>(R.id.training_id).text = training.id.value
        currentConvertView.findViewById<TextView>(R.id.start_time).text = training.start.value.toString()
        currentConvertView.findViewById<TextView>(R.id.end_time).text = training.end.value.toString()
        currentConvertView.findViewById<TextView>(R.id.courseType).text = training.course.type.value
        currentConvertView.findViewById<TextView>(R.id.courseId).text = training.course.id.value
        currentConvertView.findViewById<TextView>(R.id.courseTitle).text = training.course.title.value
        currentConvertView.findViewById<ImageView>(R.id.addAlert).setOnClickListener {
            singleAlertRepository.addOrUpdate(TrainingAndAlert(training, Alert(true)))
            currentConvertView.findViewById<ImageView>(R.id.addAlert).visibility = View.GONE
            currentConvertView.findViewById<ImageView>(R.id.removeAlert).visibility = View.VISIBLE
        }

        currentConvertView.findViewById<ImageView>(R.id.removeAlert).setOnClickListener {
            singleAlertRepository.addOrUpdate(TrainingAndAlert(training, Alert(false)))
            currentConvertView.findViewById<ImageView>(R.id.removeAlert).visibility = View.GONE
            currentConvertView.findViewById<ImageView>(R.id.addAlert).visibility = View.VISIBLE
        }

        if(trainingAndAlert.alert.isActive){
            currentConvertView.findViewById<ImageView>(R.id.addAlert).visibility = View.GONE
            currentConvertView.findViewById<ImageView>(R.id.removeAlert).visibility = View.VISIBLE
        }

        return currentConvertView
    }
}