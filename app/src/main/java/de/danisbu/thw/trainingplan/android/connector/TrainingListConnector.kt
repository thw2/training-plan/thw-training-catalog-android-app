package de.danisbu.thw.trainingplan.android.connector

import de.danisbu.thw.trainingplan.modell.TrainingId
import de.danisbu.thw.trainingplan.modell.TrainingList

interface TrainingListConnector {
    fun getFullTrainingList(): TrainingList
    fun getTrainingsForIdsIfExisting(read: List<TrainingId>): TrainingList
}