package de.danisbu.thw.trainingplan.android.persistence.io.room.entities

import androidx.room.*
import de.danisbu.thw.trainingplan.android.persistence.io.room.converter.Date
import de.danisbu.thw.trainingplan.modell.*
import kotlinx.datetime.LocalDateTime

@Entity()
@TypeConverters(Date::class)
data class TrainingEntity (
    @PrimaryKey val id: String,
    val start: LocalDateTime,
    val end: LocalDateTime,
    val registrationDeadline: LocalDateTime?,
    val courseId: String,
    val location: String,
    val registeredAlert: Boolean
)