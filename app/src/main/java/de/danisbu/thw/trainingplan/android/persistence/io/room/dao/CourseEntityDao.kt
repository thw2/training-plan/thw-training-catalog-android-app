package de.danisbu.thw.trainingplan.android.persistence.io.room.dao

import androidx.room.*
import de.danisbu.thw.trainingplan.android.persistence.io.room.entities.CourseEntity

@Dao
interface CourseEntityDao {
    @Query("SELECT * FROM CourseEntity")
    fun getAll(): List<CourseEntity>

    @Insert
    fun insert(vararg courseEntity: CourseEntity)

    @Update
    fun update(vararg courseEntity: CourseEntity)

    @Delete
    fun delete(courseEntity: CourseEntity)

    @Query("SELECT * FROM CourseEntity WHERE id LIKE :id")
    fun getByCourseId(id: String): List<CourseEntity>
}