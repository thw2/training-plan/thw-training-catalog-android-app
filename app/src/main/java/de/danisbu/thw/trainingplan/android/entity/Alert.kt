package de.danisbu.thw.trainingplan.android.entity

data class Alert (val isActive: Boolean)
