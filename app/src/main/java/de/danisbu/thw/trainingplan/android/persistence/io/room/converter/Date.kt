package de.danisbu.thw.trainingplan.android.persistence.io.room.converter

import androidx.room.TypeConverter
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.toJavaLocalDateTime
import java.time.format.DateTimeFormatter

class Date {
    @TypeConverter
    fun toDate(isoString: String?): LocalDateTime? {
        return if (isoString == null) null else LocalDateTime.parse(isoString)
    }

    @TypeConverter
    fun fromDate(date: LocalDateTime?): String? {
        return date?.toJavaLocalDateTime()?.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)
    }
}