package de.danisbu.thw.trainingplan.android.entity

data class TrainingAndAlertList(val list: List<TrainingAndAlert>)