package de.danisbu.thw.trainingplan.android.persistence

import android.content.Context
import android.util.Log
import androidx.room.Room
import androidx.room.Transaction
import de.danisbu.thw.trainingplan.android.entity.TrainingAndAlert
import de.danisbu.thw.trainingplan.android.entity.TrainingAndAlertList
import de.danisbu.thw.trainingplan.android.persistence.io.room.Database
import de.danisbu.thw.trainingplan.android.persistence.io.room.entities.CourseEntity
import de.danisbu.thw.trainingplan.android.persistence.io.room.entities.TrainingWithCourseEntity
import de.danisbu.thw.trainingplan.modell.Course
import de.danisbu.thw.trainingplan.modell.CourseId
import de.danisbu.thw.trainingplan.modell.TrainingId

class SingleAlertRepository (
    private val database: Database
) {
    companion object {
        fun createFromContext(context: Context): SingleAlertRepository {
            val db = Room.databaseBuilder(
                context,
                Database::class.java, "database-name-test"
            ).allowMainThreadQueries().build()

            db.openHelper.writableDatabase //only needed to keep database open for debug purposes

            return SingleAlertRepository(
                db
            )
        }
    }

    @Transaction //move to dao cause transaction doesnt work outside of dao
    fun addOrUpdate(trainingAndAlert: TrainingAndAlert) {
        addOrUpdateCourse(trainingAndAlert.training.course)
        addOrUpdateTraining(trainingAndAlert)
    }

    private fun addOrUpdateCourse(course: Course){
        if(this.findCourseById(course.id) != null){
            this.database.courseDao().update(
                CourseEntity.from(course)
            )
            Log.i("SingleAlertRepository", "updated course with id: " + course.id.value)
        } else {
            this.database.courseDao().insert(
                CourseEntity.from(course)
            )
            Log.i("SingleAlertRepository", "added course with id: " + course.id.value)
        }
    }

    private fun addOrUpdateTraining(trainingAndAlert: TrainingAndAlert){
        if(this.findTrainingById(trainingAndAlert.training.id) != null){
            this.database.trainingEntityDao().update(
                TrainingWithCourseEntity.from(trainingAndAlert).trainingEntity
            )
            Log.i("SingleAlertRepository", "updated training with id: " + trainingAndAlert.training.id.value)
        } else {
            this.database.trainingEntityDao().insert(
                TrainingWithCourseEntity.from(trainingAndAlert).trainingEntity
            )
            Log.i("SingleAlertRepository", "added training with id: " + trainingAndAlert.training.id.value)
        }
    }

    fun findAll(): TrainingAndAlertList {
        val trainingWithCourseEntities = this.database.trainingEntityDao().getAllTrainingEntitiesWithCourses()

        Log.i("SingleAlertRepository", "found " + trainingWithCourseEntities.size + " entries")
        return TrainingAndAlertList(
            trainingWithCourseEntities.map { it.toTrainingAndAlert() }
        )
    }

    fun findTrainingById(trainingId: TrainingId): TrainingAndAlert?{
        val trainingEntityList = database.trainingEntityDao().getByTrainingId(trainingId.value)
        if(trainingEntityList.isEmpty()){
            return null
        }
        return trainingEntityList.first().toTrainingAndAlert()
    }

    fun findCourseById(courseId: CourseId): Course?{
        val courseEntityList = database.courseDao().getByCourseId(courseId.value)
        if(courseEntityList.isEmpty()){
            return null
        }
        return courseEntityList.first().toCourse()
    }

    fun remove(trainingAndAlert: TrainingAndAlert) {
        database.trainingEntityDao().delete(TrainingWithCourseEntity.from(trainingAndAlert).trainingEntity)
    }
}