package de.danisbu.thw.trainingplan.android.persistence.io.room

import androidx.room.RoomDatabase
import de.danisbu.thw.trainingplan.android.persistence.io.room.dao.CourseEntityDao
import de.danisbu.thw.trainingplan.android.persistence.io.room.dao.TrainingEntityDao
import de.danisbu.thw.trainingplan.android.persistence.io.room.entities.CourseEntity
import de.danisbu.thw.trainingplan.android.persistence.io.room.entities.TrainingEntity

@androidx.room.Database(entities = [TrainingEntity::class, CourseEntity::class], version = 1)
abstract class Database : RoomDatabase() {
    abstract fun trainingEntityDao(): TrainingEntityDao
    abstract fun courseDao(): CourseEntityDao
}