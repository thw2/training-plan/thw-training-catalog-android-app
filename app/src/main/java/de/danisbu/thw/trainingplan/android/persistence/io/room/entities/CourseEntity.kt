package de.danisbu.thw.trainingplan.android.persistence.io.room.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import de.danisbu.thw.trainingplan.modell.Course
import de.danisbu.thw.trainingplan.modell.CourseId
import de.danisbu.thw.trainingplan.modell.Title
import de.danisbu.thw.trainingplan.modell.Type

@Entity
data class CourseEntity(
    @PrimaryKey val id: String,
    val type: String,
    val title: String
) {
    companion object {
        fun from(course: Course): CourseEntity {
            return CourseEntity(course.id.value, course.type.value, course.title.value)
        }
    }

    fun toCourse(): Course {
        return Course(Title(this.title), Type(this.type) ,CourseId(this.id))
    }
}