package de.danisbu.thw.trainingplan.android.persistence.io.room.entities

import androidx.room.Embedded
import androidx.room.Relation
import de.danisbu.thw.trainingplan.android.entity.Alert
import de.danisbu.thw.trainingplan.android.entity.TrainingAndAlert
import de.danisbu.thw.trainingplan.builder.CourseBuilder
import de.danisbu.thw.trainingplan.modell.*

data class TrainingWithCourseEntity(
    @Relation(
        parentColumn = "courseId",
        entityColumn = "id"
    )
    val courseEntity: CourseEntity,
    @Embedded
    val trainingEntity: TrainingEntity
    ) {
    companion object {
        fun from(trainingAndAlert: TrainingAndAlert): TrainingWithCourseEntity {
            val training = trainingAndAlert.training
            return TrainingWithCourseEntity(
                CourseEntity(
                    training.course.id.value,
                    training.course.type.value,
                    training.course.title.value
                ),
                TrainingEntity(
                    training.id.value,
                    training.start.value,
                    training.end.value,
                    training.registrationDeadline?.value,
                    training.course.id.value,
                    training.location.value,
                    trainingAndAlert.alert.isActive
                )
            )
        }
    }

    fun toTrainingAndAlert(): TrainingAndAlert {
        val courseBuilder = CourseBuilder(
            Title(this.courseEntity.title),
            Type(this.courseEntity.type),
            CourseId(this.courseEntity.id)
        )

        val training = Training(
            TrainingId(this.trainingEntity.id),
            Start(this.trainingEntity.start),
            End(this.trainingEntity.end),
            if(this.trainingEntity.registrationDeadline !== null) RegistrationDeadline(this.trainingEntity.registrationDeadline) else null,
            courseBuilder.built(),
            Location(this.trainingEntity.location)
        )

        return TrainingAndAlert(
            training,
            Alert(this.trainingEntity.registeredAlert)
        )
    }
}