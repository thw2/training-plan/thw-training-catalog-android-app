package de.danisbu.thw.trainingplan.android.entity

import de.danisbu.thw.trainingplan.modell.Training

data class TrainingAndAlert (val training: Training, val alert: Alert)