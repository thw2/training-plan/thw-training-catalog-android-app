package de.danisbu.thw.trainingplan.android

import de.danisbu.thw.trainingplan.modell.*
import kotlinx.datetime.toKotlinLocalDateTime
import java.time.LocalDateTime

open class ConnectorTest {
    val expected = TrainingList(
        mutableListOf(
            Training(
                TrainingId("H 234/22"),
                Start(LocalDateTime.of(2022, 8, 22, 8, 0, 0, 0).toKotlinLocalDateTime()),
                End(LocalDateTime.of(2022, 8, 24, 12, 0, 0, 0).toKotlinLocalDateTime()),
                RegistrationDeadline(LocalDateTime.of(2022, 6, 29, 0, 0, 0, 0).toKotlinLocalDateTime()),
                Course(
                    Title("Systemadministration im OV"),
                    Type("Ausb"),
                    CourseId("11")
                ),
                Location("Hoya")
            ),
            Training(
                TrainingId("NI 12/22"),
                Start(LocalDateTime.of(2022, 8, 22, 8, 0, 0, 0).toKotlinLocalDateTime()),
                End(LocalDateTime.of(2022, 8, 26, 9, 30, 0, 0).toKotlinLocalDateTime()),
                RegistrationDeadline(LocalDateTime.of(2022, 6, 27, 0, 0, 0, 0).toKotlinLocalDateTime()),
                Course(
                    Title("Thermisches Trennen im THW"),
                    Type("Spez"),
                    CourseId("10")
                ),
                Location("Nienburg (Weser)")
            ),
            Training(
                TrainingId("N 116/22"),
                Start(LocalDateTime.of(2022, 8, 22, 8, 0, 0, 0).toKotlinLocalDateTime()),
                End(LocalDateTime.of(2022, 8, 26, 11, 0, 0, 0).toKotlinLocalDateTime()),
                RegistrationDeadline(LocalDateTime.of(2022, 6, 27, 0, 0, 0, 0).toKotlinLocalDateTime()),
                Course(
                    Title("Grundlagen Führung"),
                    Type("FüUF"),
                    CourseId("26")
                ),
                Location("Neuhausen a.d.F.")
            ),
            Training(
                TrainingId("H 259/22"),
                Start(LocalDateTime.of(2022, 9, 19, 8, 0, 0, 0).toKotlinLocalDateTime()),
                End(LocalDateTime.of(2022, 9, 23, 10, 30, 0, 0).toKotlinLocalDateTime()),
                null,
                Course(
                    Title("Grundlagen Führung"),
                    Type("FüUF"),
                    CourseId("26")
                ),
                Location("Hoya")
            )
        )
    )
}
