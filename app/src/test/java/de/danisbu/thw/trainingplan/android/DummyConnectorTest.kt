package de.danisbu.thw.trainingplan.android

import de.danisbu.thw.trainingplan.android.connector.DummyConnector
import de.danisbu.thw.trainingplan.modell.*
import kotlinx.datetime.toKotlinLocalDateTime
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is.`is`
import org.junit.Test
import java.time.LocalDateTime

class DummyConnectorTest: ConnectorTest() {
    @Test
    fun testIfDummyConnectorReturnsCorrectFullList() {
        assertThat(DummyConnector().getFullTrainingList(), `is`(expected))
    }

    @Test
    fun testIfDummyConnectorReturnsEmptyTrainingListIfEmptyTrainingIdListIsInserted(){
        assertThat(DummyConnector().getTrainingsForIdsIfExisting(listOf()), `is`(TrainingList(mutableListOf())))
    }

    @Test
    fun testIfDummyConnectorReturnsCorrectTrainingListForInsertedTrainingIdList(){
        assertThat(
            DummyConnector().getTrainingsForIdsIfExisting(
                listOf(TrainingId("N 116/22"), TrainingId("H 259/22"))
            ), `is`(
                TrainingList(mutableListOf(Training(
            TrainingId("N 116/22"),
            Start(LocalDateTime.of(2022, 8, 22, 8, 0, 0, 0).toKotlinLocalDateTime()),
            End(LocalDateTime.of(2022, 8, 26, 11, 0, 0, 0).toKotlinLocalDateTime()),
            RegistrationDeadline(LocalDateTime.of(2022, 6, 27, 0, 0, 0, 0).toKotlinLocalDateTime()),
            Course(
                Title("Grundlagen Führung"),
                Type("FüUF"),
                CourseId("26")
            ),
            Location("Neuhausen a.d.F.")
        ),
            Training(
                TrainingId("H 259/22"),
                Start(LocalDateTime.of(2022, 9, 19, 8, 0, 0, 0).toKotlinLocalDateTime()),
                End(LocalDateTime.of(2022, 9, 23, 10, 30, 0, 0).toKotlinLocalDateTime()),
                null,
                Course(
                    Title("Grundlagen Führung"),
                    Type("FüUF"),
                    CourseId("26")
                ),
                Location("Hoya")
            )))))
    }

    @Test
    fun testIfDummyConnectorReturnsCorrectTrainingListForInsertedTrainingIdListIfOneTrainingIdDoesntExistAnymore(){
        assertThat(
            DummyConnector().getTrainingsForIdsIfExisting(
                listOf(TrainingId("N 116/22s"), TrainingId("H 259/22"))
            ), `is`(
                TrainingList(mutableListOf(
                    Training(
                        TrainingId("H 259/22"),
                        Start(LocalDateTime.of(2022, 9, 19, 8, 0, 0, 0).toKotlinLocalDateTime()),
                        End(LocalDateTime.of(2022, 9, 23, 10, 30, 0, 0).toKotlinLocalDateTime()),
                        null,
                        Course(
                            Title("Grundlagen Führung"),
                            Type("FüUF"),
                            CourseId("26")
                        ),
                        Location("Hoya")
            )))))
    }
}