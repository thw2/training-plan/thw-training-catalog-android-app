package de.danisbu.thw.trainingplan.android

import android.view.View
import android.widget.ListView
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import de.danisbu.thw.trainingplan.android.entity.TrainingAndAlert
import de.danisbu.thw.trainingplan.android.view.ListActivity
import org.hamcrest.CoreMatchers
import org.hamcrest.Description
import org.hamcrest.TypeSafeMatcher
import org.hamcrest.core.AllOf
import org.hamcrest.core.Is
import org.hamcrest.core.IsInstanceOf
import org.junit.Assert
import org.junit.Rule
import org.junit.Test


@LargeTest
class ListActivityTest {
    @get:Rule
    val activityRule = ActivityScenarioRule(ListActivity::class.java)

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        Assert.assertEquals("de.danisbu.thw.trainingplan.android", appContext.packageName)
    }

    @Test
    fun checkListIsDisplayed() {
        onView(withId(R.id.training_list_view)).check(matches(isDisplayed()))
    }

    @Test
    fun checkTitleContainsCorrectValue() {
        var count = 0

        val matcher = object : TypeSafeMatcher<View?>() {
            override fun matchesSafely(item: View?): Boolean {
                count = (item as ListView).count

                return true
            }

            override fun describeTo(description: Description?) {
                description?.appendText ("Training List should have $count items")
            }
        }

        onView(withId(R.id.training_list_view)).check(matches(matcher))

        Assert.assertEquals(4, count)
    }

    @Test
    fun listItem(){
        val listItem = Espresso.onData(AllOf.allOf(Is.`is`(IsInstanceOf.instanceOf(TrainingAndAlert::class.java))))
            .atPosition(0)

        listItem.onChildView(withId(R.id.courseTitle))
            .check(
                matches(
                    withText(CoreMatchers.containsString("Systemadministration im OV"))
                )
            )

        listItem.onChildView(withId(R.id.courseType))
            .check(
                matches(
                    withText(CoreMatchers.containsString("Ausb"))
                )
            )

        listItem.onChildView(withId(R.id.courseId))
            .check(
                matches(
                    withText(CoreMatchers.containsString("11"))
                )
            )

        listItem.onChildView(withId(R.id.location))
            .check(
                matches(
                    withText(CoreMatchers.containsString("Hoya"))
                )
            )

        listItem.onChildView(withId(R.id.training_id))
            .check(
                matches(
                    withText(CoreMatchers.containsString("H 234/22"))
                )
            )

        listItem.onChildView(withId(R.id.start_time))
            .check(
                matches(
                    withText(CoreMatchers.containsString("2022-08-22T08:00"))
                )
            )

        listItem.onChildView(withId(R.id.end_time))
            .check(
                matches(
                    withText(CoreMatchers.containsString("2022-08-24T12:00"))
                )
            )


    }
}