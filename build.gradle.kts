// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {
    repositories {
        google()
        mavenCentral()
    }
    dependencies {
        classpath("com.android.tools.build:gradle:8.0.2")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.8.22")
        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}

allprojects {
    repositories {
        google()
        mavenCentral()

        maven {
            url = uri("https://gitlab.com/api/v4/projects/40318647/packages/maven")
            name = "GitLab"
            credentials(HttpHeaderCredentials::class) {
                name = findProperty("tokenType").toString()
                value = findProperty("gradleRepoAccessToken").toString()
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}

tasks{
    register("clean", Delete::class) {
        delete(rootProject.buildDir)
    }
}